﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Zadanie 1</title>
</head>
<body>
		<form action="hello" method="post">
			<label>Wnioskowana kwota kredytu: <input type="text" name="kwotaKredytu" required /></label><br />
			<label>Ilość rat: <input type="text" name="iloscRat" required /></label><br />
			<label>Oprocentowanie: <input type="text" name="oprocentowanie" required /></label><br />
			<label>Opłata stała: <input type="text" name="oplataStala" required /></label><br />
			<label>Rodzaj rat: <select name="rodzajRat"><option value="malejaca">Malejąca</option><option value="stala">Stała</option></select></label>
			<input type="submit" value="Wyślij"/><button type="submit" name="pdf" value="pdf">Export do PDF</button>
		</form>
</body>
</html>