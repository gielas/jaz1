import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import servlets.HelloServlet;

public class TestHelloServlet extends Mockito{
	
	@Test
	public void servlet_should_not_redirect_to_hello_if_kwotaKredytu_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("kwotaKredytu")).thenReturn(null);
        
        servlet.doPost(request, response);
        
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_redirect_to_hello_if_iloscRat_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("iloscRat")).thenReturn(null);
        
        servlet.doPost(request, response);
        
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_redirect_to_hello_if_oprocentowanie_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("oprocentowanie")).thenReturn(null);
        
        servlet.doPost(request, response);
        
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_redirect_to_hello_if_oplataStala_is_null() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("oplataStala")).thenReturn(null);
        
        servlet.doPost(request, response);
        
        verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_return_value_malejaca_or_rosnaca() throws IOException  {
		HttpServletRequest request = mock(HttpServletRequest.class);  
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
        when(response.getWriter()).thenReturn(writer);
		HelloServlet servlet = new HelloServlet();
		
        when(request.getParameter("rodzajRat")).thenReturn(null);
        
        servlet.doPost(request, response);
        
        verify(response).sendRedirect("/");
	}	
}